### 3.2密码操作
>
>
>
>加密手段：对称加密和非对称加密
>对称加密：加密和解密的密码是一样的，DES,AES,路由器的密码WPA/WPA2
>非对称：加密和解密的密码不一样，分为一对，RAS,DSA,分为公钥和私钥，https的证书，大素数碰撞
>
>
##服务器获取数据
##jsp的流程
>jsp是一个servlet,服务器会解析jsp,将数据解析成html,到了浏览器,浏览器解析的是html的内容，没有EL表达式的内容，而是正式内容
>没jsp怎样返回数据，用js的ajax,返回json数据，页面可以解析json中的数据展示给页面（浏览器）
>服务器只返回数据就行
>

##线程阻塞的具体地方

>线程阻塞可以使用MQ进行异步解耦，再通过推送服务推送最终执行结果。
>

##网景公司的请求头  致敬
>user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.110 Safari/537.36
>
>
##优先级队列，见CompletableFuture文档
##Spring WebFlux响应式编程，全是异步
##面向对象思想：由面向过程的执行者变为调用者的思想的转变，称为面向对象思想


###hashSet底层是hashMap存数据时，需要通过hashcode算出位置存放进去，遍历的时候，hashMap底层是数组，所以有序


####成员变量只初始化一次，成员变量有线程安全问题，为了保证安全应该使其成为局部变量

##heluoluo  12.0.0.1  yanxujia   localhost

## 编程
1.面向对象编程  OOP
2.面向接口编程
3.面向切面编程
4.面向注解编程