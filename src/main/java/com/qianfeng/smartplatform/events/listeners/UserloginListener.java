package com.qianfeng.smartplatform.events.listeners;

import com.google.common.collect.HashMultimap;
import com.qianfeng.smartplatform.events.UserLoginEvent;
import com.qianfeng.smartplatform.mapper.UserOnlineMapper;
import com.qianfeng.smartplatform.pojo.User;
import com.qianfeng.smartplatform.pojo.UserOnline;
import com.qianfeng.smartplatform.service.UserService;
import com.qianfeng.smartplatform.utils.RequestUtil;
import com.qianfeng.smartplatform.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2022/1/4 16:43
 *@Description:
 */
@Component
public class UserloginListener {

    private UserOnlineMapper userOnlineMapper;

    @Autowired
    public void setUserOnlineMapper(UserOnlineMapper userOnlineMapper) {
        this.userOnlineMapper = userOnlineMapper;
    }

    private UserService userService;

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }




    /**
     * 更新用户的基本登录信息
     *
     * @param userLoginEvent
     */
    @EventListener
    //@Async
    public void upDateUserInfo(UserLoginEvent userLoginEvent) {

/*        try {

        } catch (Exception e) {
            e.printStackTrace();
        }
        会捕获异常，不往外抛。MyControllerAdvices类中的public R processMyBaseException(Exception e)方法就不会执行， 异常会显示到控制台上
        */
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        User user = SecurityUtils.getUser();
        //通过异步执行当前操作，内部有自己的线程池
//()->{}相当于重写的run方法
        CompletableFuture.runAsync(() -> {//runAsync是不需要返回结果的异步
            try {

                //Map<String, String> osAndBrowserInfo = RequestUtil.getOsAndBrowserInfo(request);//存放的是请求的系统和软件信息
                String ip = RequestUtil.getRemoteHost(request);
                //user.setLastLogin(user.getCurrentLogin());
                user.setCurrentLogin(new Date());
                //user.setLastLoginIp(user.getCurrentLoginIp());
                user.setCurrentLoginIp(ip);
                //int i=1/0;
                userService.updateUserLogin(user);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

/* // new Thread(){}子线程
        new Thread(){
            @Override
            public void run() {
                try {

                    //Map<String, String> osAndBrowserInfo = RequestUtil.getOsAndBrowserInfo(request);//存放的是请求的系统和软件信息
                    String ip = RequestUtil.getRemoteHost(request);
                    //user.setLastLogin(user.getCurrentLogin());
                    user.setCurrentLogin(new Date());
                    //user.setLastLoginIp(user.getCurrentLoginIp());
                    user.setCurrentLoginIp(ip);
                    //int i=1/0;
                    userService.updateUserLogin(user);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }.start();*/
    }


    /**
     * 更新用户的在线状态
     *
     * @param userLoginEvent
     */

    @EventListener
    // @Async
    public void upDateUserOnline(UserLoginEvent userLoginEvent) {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
        User user = SecurityUtils.getUser();
        CompletableFuture.runAsync(() -> {
            try {
                Map<String, String> osAndBrowserInfo = RequestUtil.getOsAndBrowserInfo(request);//存放的是请求的系统和软件信息
                String ip = RequestUtil.getRemoteHost(request);//获取到用户的ip
                UserOnline userOnline = new UserOnline();
                userOnline.setSessionId(request.getSession().getId());
                userOnline.setLoginName(user.getUsername());
                userOnline.setStartTimestamp(new Date());
                userOnline.setStatus("1");//设置为在线
                userOnline.setIpaddr(ip);
                userOnline.setLoginLocation(RequestUtil.getLocationByIp(ip));//请求第三方服务器，需要时间，所以应该放到单独的线程中执行
                userOnline.setOs(osAndBrowserInfo.get("os"));
                userOnline.setBrowser(osAndBrowserInfo.get("browser"));
                userOnline.setExpireTime(30L);
                //保存到数据库，发现有问题，当前用户的数据如果已经存在了  应该更新，先查询一下数据库中是否有当前要添加的在线登录记录
                UserOnline currentLogin = userOnlineMapper.selectByLoginName(user.getUsername());
                if (currentLogin != null) {
                    //存在，删除
                    userOnlineMapper.deleteByLoginName(user.getUsername());
                }
                    //不存在，插入到数据库
                userOnlineMapper.addOnlineData(userOnline);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

    }

    /**
     * 更新登录日志
     *
     * @param userLoginEvent
     */
    @EventListener
    // @Async
    public void onLoginLog(UserLoginEvent userLoginEvent) {
        //TODO  自己写
    }

}
