package com.qianfeng.smartplatform.pojo;


import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

public class Device implements CheckNull {

    private String deviceId;
    private String deviceName;
    private Long categyId;
    private Long bindUserId;
    private Long sceneId;
    private java.util.Date bindTime;
    private Long isOnline;
    private java.util.Date connectTime;
    private java.util.Date lostConnectTime;
    private java.util.Date lastControlTime;
    private String currentConnectIp;
    private String connectLocation;

    @Override
    public boolean isEmpty(CheckType type) {
        switch (type) {
            case ADD:
                return !StringUtils.hasText(deviceId) || !StringUtils.hasText(deviceName) || ObjectUtils.isEmpty(categyId) || categyId < 0;
        }
        return false;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }


    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }


    public Long getCategyId() {
        return categyId;
    }

    public void setCategyId(Long categyId) {
        this.categyId = categyId;
    }


    public Long getBindUserId() {
        return bindUserId;
    }

    public void setBindUserId(Long bindUserId) {
        this.bindUserId = bindUserId;
    }


    public Long getSceneId() {
        return sceneId;
    }

    public void setSceneId(Long sceneId) {
        this.sceneId = sceneId;
    }


    public java.util.Date getBindTime() {
        return bindTime;
    }

    public void setBindTime(java.util.Date bindTime) {
        this.bindTime = bindTime;
    }


    public Long getIsOnline() {
        return isOnline;
    }

    public void setIsOnline(Long isOnline) {
        this.isOnline = isOnline;
    }


    public java.util.Date getConnectTime() {
        return connectTime;
    }

    public void setConnectTime(java.util.Date connectTime) {
        this.connectTime = connectTime;
    }


    public java.util.Date getLostConnectTime() {
        return lostConnectTime;
    }

    public void setLostConnectTime(java.util.Date lostConnectTime) {
        this.lostConnectTime = lostConnectTime;
    }


    public java.util.Date getLastControlTime() {
        return lastControlTime;
    }

    public void setLastControlTime(java.util.Date lastControlTime) {
        this.lastControlTime = lastControlTime;
    }


    public String getCurrentConnectIp() {
        return currentConnectIp;
    }

    public void setCurrentConnectIp(String currentConnectIp) {
        this.currentConnectIp = currentConnectIp;
    }


    public String getConnectLocation() {
        return connectLocation;
    }

    public void setConnectLocation(String connectLocation) {
        this.connectLocation = connectLocation;
    }

}
