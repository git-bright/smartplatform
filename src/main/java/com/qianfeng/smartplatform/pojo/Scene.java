package com.qianfeng.smartplatform.pojo;


import org.springframework.util.StringUtils;

public class Scene implements CheckNull {

  private Long sceneId;
  private String sceneName;
  private Long createBy;
  private java.util.Date createTime;
  private Long status;

  @Override
  public boolean isEmpty(CheckType type) {
    return !StringUtils.hasText(sceneName);
  }

  public Long getSceneId() {
    return sceneId;
  }

  public void setSceneId(Long sceneId) {
    this.sceneId = sceneId;
  }


  public String getSceneName() {
    return sceneName;
  }

  public void setSceneName(String sceneName) {
    this.sceneName = sceneName;
  }


  public Long getCreateBy() {
    return createBy;
  }

  public void setCreateBy(Long createBy) {
    this.createBy = createBy;
  }


  public java.util.Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.util.Date createTime) {
    this.createTime = createTime;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }

}
