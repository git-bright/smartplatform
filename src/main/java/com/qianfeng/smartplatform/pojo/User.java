package com.qianfeng.smartplatform.pojo;


import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

public class User implements CheckNull {

  private Long uId;
  private String username;
  private String password;
  private String pwdSalt;
  private String name;
  private String phone;
  private String email;
  private Long sex;
  private String avator;
  private String info;
  private Long type;
  private Long status;
  private java.util.Date currentLogin;
  private java.util.Date lastLogin;
  private String currentLoginIp;
  private String lastLoginIp;
  private java.util.Date createTime;
  private String createBy;
  private java.util.Date updateTime;
  private String updateBy;
  private String remark;


  @Override
  public boolean isEmpty(CheckType type) {
    switch (type){
      case ADD:
        return !StringUtils.hasText(username)
                ||!StringUtils.hasText(password)
                ||!StringUtils.hasText(name)
                ||!StringUtils.hasText(phone)
                ||!StringUtils.hasText(email)//hasText表示不为空时返回true
                ||ObjectUtils.isEmpty(sex)||sex<0;//isEmpty表示为空返回true
    }
    return CheckNull.super.isEmpty(type);
  }

  public Long getUId() {
    return uId;
  }

  public void setUId(Long uId) {
    this.uId = uId;
  }


  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }


  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }


  public String getPwdSalt() {
    return pwdSalt;
  }

  public void setPwdSalt(String pwdSalt) {
    this.pwdSalt = pwdSalt;
  }


  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }


  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }


  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }


  public Long getSex() {
    return sex;
  }

  public void setSex(Long sex) {
    this.sex = sex;
  }


  public String getAvator() {
    return avator;
  }

  public void setAvator(String avator) {
    this.avator = avator;
  }


  public String getInfo() {
    return info;
  }

  public void setInfo(String info) {
    this.info = info;
  }


  public Long getType() {
    return type;
  }

  public void setType(Long type) {
    this.type = type;
  }


  public Long getStatus() {
    return status;
  }

  public void setStatus(Long status) {
    this.status = status;
  }


  public java.util.Date getCurrentLogin() {
    return currentLogin;
  }

  public void setCurrentLogin(java.util.Date currentLogin) {
    this.currentLogin = currentLogin;
  }


  public java.util.Date getLastLogin() {
    return lastLogin;
  }

  public void setLastLogin(java.util.Date lastLogin) {
    this.lastLogin = lastLogin;
  }


  public String getCurrentLoginIp() {
    return currentLoginIp;
  }

  public void setCurrentLoginIp(String currentLoginIp) {
    this.currentLoginIp = currentLoginIp;
  }


  public String getLastLoginIp() {
    return lastLoginIp;
  }

  public void setLastLoginIp(String lastLoginIp) {
    this.lastLoginIp = lastLoginIp;
  }


  public java.util.Date getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.util.Date createTime) {
    this.createTime = createTime;
  }


  public String getCreateBy() {
    return createBy;
  }

  public void setCreateBy(String createBy) {
    this.createBy = createBy;
  }


  public java.util.Date getUpdateTime() {
    return updateTime;
  }

  public void setUpdateTime(java.util.Date updateTime) {
    this.updateTime = updateTime;
  }


  public String getUpdateBy() {
    return updateBy;
  }

  public void setUpdateBy(String updateBy) {
    this.updateBy = updateBy;
  }


  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

}
