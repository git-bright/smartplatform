package com.qianfeng.smartplatform.pojo;


import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

public class Category implements CheckNull {

    private Long cId;
    private String categoryName;
    private String txCommand;
    private String rxCommand;
    private String commandName;
    private Long status;
    private java.util.Date createTime;
    private String createBy;
    private java.util.Date updateTime;
    private String updateBy;

    @Override
    public boolean isEmpty(CheckType type) {
        switch (type) {
            case ADD://添加的时候，大部分数据都可以为空，唯独分类的名字不能为空
                return !StringUtils.hasText(categoryName);
            case UPDATE:
                return (ObjectUtils.isEmpty(cId) || cId <= 0) ||
                        (StringUtils.hasText(categoryName)
                                && !StringUtils.hasText(txCommand)
                                && !StringUtils.hasText(rxCommand)
                                && !StringUtils.hasText(commandName)
                                && (ObjectUtils.isEmpty(status) || status < 0));
        }
        return CheckNull.super.isEmpty(type);
    }

    public Long getCId() {
        return cId;
    }

    public void setCId(Long cId) {
        this.cId = cId;
    }


    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }


    public String getTxCommand() {
        return txCommand;
    }

    public void setTxCommand(String txCommand) {
        this.txCommand = txCommand;
    }


    public String getRxCommand() {
        return rxCommand;
    }

    public void setRxCommand(String rxCommand) {
        this.rxCommand = rxCommand;
    }


    public String getCommandName() {
        return commandName;
    }

    public void setCommandName(String commandName) {
        this.commandName = commandName;
    }


    public Long getStatus() {
        return status;
    }

    public void setStatus(Long status) {
        this.status = status;
    }


    public java.util.Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(java.util.Date createTime) {
        this.createTime = createTime;
    }


    public String getCreateBy() {
        return createBy;
    }

    public void setCreateBy(String createBy) {
        this.createBy = createBy;
    }


    public java.util.Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(java.util.Date updateTime) {
        this.updateTime = updateTime;
    }


    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

}
