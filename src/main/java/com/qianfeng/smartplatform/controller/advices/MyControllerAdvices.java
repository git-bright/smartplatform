package com.qianfeng.smartplatform.controller.advices;

import com.qianfeng.smartplatform.dto.R;
import com.qianfeng.smartplatform.exceptions.MyBaseException;
import com.qianfeng.smartplatform.exceptions.ResultCode;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2021/12/29 22:11
 *@Description:
 */

@ControllerAdvice//声明当前类专门拦截controller,这个注解是spring实现的
@Component//由spring创建该对象
@ResponseBody
public class MyControllerAdvices {
    @ExceptionHandler(MyBaseException.class)//声明当前注解是专门处理MyBaseException类型的异常
    public R processMyBaseException(MyBaseException e) {
        //e.printStackTrace();
        int code = e.getCode();
        String message = e.getMessage();
        return R.setError(code, message);
    }

    /**
     * 我们发现除了上面的异常之外我们可能还会遇到未知的异常，也需要处理，以为是未知异常，所以直接返回失败
     *
     * @param e
     * @return
     */
    @ExceptionHandler(Exception.class)//声明当前注解是专门处理Exception类型的异常
    public R processMyBaseException(Exception e) {
        //e.getStackTrace();
        return R.setError(ResultCode.FAIL, "请检查您的网络", null);
    }
}
