package com.qianfeng.smartplatform.controller;

import com.qianfeng.smartplatform.cache.CategoryCache;
import com.qianfeng.smartplatform.dto.R;
import com.qianfeng.smartplatform.pojo.Category;
import com.qianfeng.smartplatform.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2021/12/30 20:42
 *@Description:
 */
@RestController
@RequestMapping("/categories")
public class CategoryController {
    private CategoryService categoryService;

    @Autowired
    public void setCategoryService(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/addcategory")
    public R addCategory(@RequestBody Category category) {
        categoryService.addCategory(category);
        return R.setOK();
    }

    @GetMapping("/categories")
    public R getAllCategories() {
        List<Category> categoryList = categoryService.selectAllCategories();
        return R.setOK(categoryList);
    }

    @GetMapping("/category/{id}")
    public R getCategoryById(@PathVariable Long id) {
        Category category = categoryService.selectById(id);
        return R.setOK(category);
    }

    @GetMapping("/category")
    public R getCategoryByName(String cName) {
        List<Category> categoryList = categoryService.selectCategoriesByNameLike(cName);
        return R.setOK(categoryList);
    }

    @DeleteMapping("/deletecategory")
    public R deleteCategory(@RequestBody List<Long> ids) {
        int result = categoryService.deleteCategoriesByIds(ids);
        return R.setOK();
    }
}
