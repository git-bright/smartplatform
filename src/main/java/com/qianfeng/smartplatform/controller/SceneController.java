package com.qianfeng.smartplatform.controller;

import com.qianfeng.smartplatform.dto.R;
import com.qianfeng.smartplatform.pojo.Scene;
import com.qianfeng.smartplatform.pojo.User;
import com.qianfeng.smartplatform.service.SceneService;
import com.qianfeng.smartplatform.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2022/1/8 16:40
 *@Description:
 */
@RestController
@RequestMapping("/scenes")
public class SceneController {
    private SceneService sceneService;

    @Autowired
    public void setSceneService(SceneService sceneService) {
        this.sceneService = sceneService;
    }

    @PostMapping("/scene")
    public R addScene(@RequestBody Scene scene) {
        sceneService.addScene(scene);
        return R.setOK();
    }
    @GetMapping("/scenes")
    public R findAllSceneByUser(){
        User user = SecurityUtils.getUser();
        List<Scene> sceneList = sceneService.findByUserId(user.getUId());
        return R.setOK(sceneList);
    }
}
