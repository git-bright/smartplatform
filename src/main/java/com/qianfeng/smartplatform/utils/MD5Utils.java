package com.qianfeng.smartplatform.utils;

import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2021/12/28 20:52
 *@Description:
 */
public class MD5Utils {

    private static String zifu = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private static List chars = new ArrayList();

    static {//static中的的代码是保证程序一启动起来就能执行
        //因为用户输入密码的时间，有可能是程序启动起来就输入了密码，所以盐要在程序启动起来就有
        char[] charArray = zifu.toCharArray();
//         chars= Arrays.asList(charArray);//将数组转成集合，但是下面遍历集合的时候打印出来的数据是一个一个的字符串数组不是我们想要的，我们要的是集合里的一分一分字符。所以需要遍历数组
        for (char c : charArray) {
            chars.add(c);
        }
    }

    public static String getRandomString(int size) {
        Collections.shuffle(chars);
        Random random = new Random();
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < size; i++) {
            int index = random.nextInt(chars.size());
            stringBuffer.append(chars.get(index).toString());

        }
        return stringBuffer.toString();
    }

    public static String getMD5(String source,String salt,int times){

        if (times==1) {
            return DigestUtils.md5DigestAsHex((salt+source+salt).getBytes(StandardCharsets.UTF_8));
        }
        else {
            --times;
            return getMD5(getMD5(source, salt, 1),source,times);
        }
    }


}

