package com.qianfeng.smartplatform.service.impl;

import com.qianfeng.smartplatform.cache.DefaultRoleCache;
import com.qianfeng.smartplatform.cache.RoleCache;
import com.qianfeng.smartplatform.events.UserLoginEvent;
import com.qianfeng.smartplatform.exceptions.MyBaseException;
import com.qianfeng.smartplatform.exceptions.QueryDataException;
import com.qianfeng.smartplatform.exceptions.ResultCode;
import com.qianfeng.smartplatform.mapper.DefaultRoleMapper;
import com.qianfeng.smartplatform.mapper.RoleMapper;
import com.qianfeng.smartplatform.mapper.UserMapper;
import com.qianfeng.smartplatform.pojo.*;
import com.qianfeng.smartplatform.service.UserService;
import com.qianfeng.smartplatform.utils.MD5Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.List;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2021/12/27 19:57
 *@Description:
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {
    private ApplicationContext context;

    @Autowired
    public void setContext(ApplicationContext context) {
        this.context = context;
    }

    private UserMapper userMapper;

    @Autowired
    public void setUserMapper(UserMapper userMapper) {
        this.userMapper = userMapper;
    }


    private DefaultRoleCache defaultRoleCache;

    @Autowired
    public void setDefaultRoleCache(DefaultRoleCache defaultRoleCache) {
        this.defaultRoleCache = defaultRoleCache;
    }

    private RoleCache roleCache;

    @Autowired
    public void setRoleCache(RoleCache roleCache) {
        this.roleCache = roleCache;
    }
    /*    private DefaultRoleMapper defaultRoleMapper;

    @Autowired
    public void setDefaultRoleMapper(DefaultRoleMapper defaultRoleMapper) {
        this.defaultRoleMapper = defaultRoleMapper;
    }

    private RoleMapper roleMapper;

    @Autowired
    public void setRoleMapper(RoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }*/


    @Override
    public void addUser(User user) {


//        1.用户名允许为空吗？

/*        if (StringUtils.hasText(user.getUsername())) {//数据不能是null和""包括" ",要求至少存放一个有效字符
            //返回，用户没有传递必须传递的数据，我们要抛出异常，抛什么异常，随便一个异常？我们发现我们需要携带数据出去，所以必须抛出自定义异常
            throw new MyBaseException("传递的用户名是空的", ResultCode.USERNAME_NULL);
    }*/


/*        if (user.isEmpty(CheckType.ADD)) {
            //一定代表着有数据没有传递
            throw new MyBaseException("传递的用户名是空的", ResultCode.USERNAME_NULL);
        }*/
//        2.密码允许为空吗？
//断言，我断言这个user一定符合我们的要求，只要不符合我就扔异常
//        isEmpty()中return false;返回的是false,代表不为空，!user.isEmpty(CheckType.ADD)
        Assert.isTrue(!user.isEmpty(CheckType.ADD), () -> {
            throw new MyBaseException("传递的用户名是空的", ResultCode.USERNAME_NULL);
        });

//        第二步 判断数据是不是已经存在
//        用户名允许重复吗？
//        手机号允许重复吗？
//        邮箱允许重复吗？
//        等等


        //根据用户名或者手机号或者邮箱查询用户
        List<User> currentUser = userMapper.selectByUserNameOrPhoneOrEmail(user.getUsername(), user.getPhone(), user.getEmail());
        //上面的对象必须是空的，不为空就说明数据已经存在
//        if (currentUser!=null) {
//            //抛出异常
//        }

        Assert.isTrue(currentUser.size() == 0, () -> {
            throw new MyBaseException("账号已经存在", ResultCode.DATA_ALREADY_EXIST);
        });

//        我们已经能得知的异常能提前处理的就必须要提前处理
//        前段所有的数据都不可信，前段所有的校验都属于无效校验，前端的校验属于可更改操作，服务端必须校验


//        对数据进行操作，比如密码需要进行"加密"，MD SHA 都不是加密，基于消息摘要算法实现的校验手段（散列值）
//        加密必须能反向解密。MD SHA 有非常大的熵增，数据丢失  100G的文件  32位字符串
//        非加密算法
        //处理密码，将密码改成MD5值
        String password = user.getPassword();
//        String passwordMD5 = DigestUtils.md5DigestAsHex(password.getBytes(StandardCharsets.UTF_8));
//计算md5值时，相同的密码总是会得到相同的值，万一数据泄露，还是很容易被反向得到密码，所以需要加盐
        String salt = MD5Utils.getRandomString(15);

        String md5 = MD5Utils.getMD5(password, salt, 1024);
        user.setPassword(md5);
        user.setPwdSalt(salt);
        user.setCreateTime(new Date());
        user.setCreateBy(user.getUsername());

//        数据库是不支持高并发的，你搞了一堆的错误进去，不占用数据库的io吗？
/*        Long roleId = defaultRoleMapper.selectRoleIdByType("user");
        Role role = roleMapper.selectRoleById(roleId);*/
        DefaultRole defaultRole = defaultRoleCache.getValue("user");
//        Long roleId = defaultRole.getRoleId();
        Role role = roleCache.getValue(defaultRole.getRoleId());
        user.setRemark(role.getRemark());

        userMapper.addUser(user);
    }

    @Override
    public User login(String username, String password) {

        //我们判断的标准是用户输入的数据和数据库的用户的数据进行比较，如果一致就通过
        //查询数据库，用什么条件查询？账号查询，账号有唯一索引，密码没有
        //我们要求必须传递账号和密码
        Assert.isTrue(StringUtils.hasText(username) && StringUtils.hasText(password), () -> {
            throw new QueryDataException("账号或者密码错误", ResultCode.USERNAME_PASSWORD_ERROR);
        });

        User user = userMapper.selectUserByUserName(username);
        //如果查询不到，就返回一个null对象
        //只有查询不到就一定是账号错误
        Assert.notNull(user, () -> {
            throw new QueryDataException("账号或者密码错误", ResultCode.USERNAME_PASSWORD_ERROR);

        });

        //有结果就要比较密码
        //但是用户传递的密码是明文，数据库是md5值，直接比较一定会失败
        //因为MD5不可逆，所以我们只能选择把用户传递的密码再次转成MD5值，然后进行比较，但是转换的时候需要盐
        //盐在用户数据中，我们需要获取，然后用一开始注册时候生成MD5的操作重新操作一遍
        String pwdSalt = user.getPwdSalt();//盐
        String pwdmd5 = MD5Utils.getMD5(password, pwdSalt, 1024);//计算出用户现在传递的密码的MD5
        Assert.isTrue(pwdmd5.equalsIgnoreCase(user.getPassword()), () -> {
            throw new QueryDataException("账号或者密码错误", ResultCode.USERNAME_PASSWORD_ERROR);
        });

        //说明用户传递的账号和密码是对的
        user.setPassword(null);
        user.setPwdSalt(null);
        //发送事件，更新用户的一些登录后的数据,谁接收呢？没人接收自己创建一个监听类接收
        //因为取消了@Async异步，事件没有处理完，不会执行下面 return user; 这行代码，没有返回数据，所以接收事件那儿块获取不到user,所以要放到已经返回user数据后发送事件，controller
        //context.publishEvent(new UserLoginEvent());
        return user;
    }

    @Override
    public int updateUserLogin(User user) {
        //判断时间，不判断，因为这个数据不是用户传递的，是我们自己生成的，我们只要确保一定生成就行
        userMapper.upDateUserLogin(user);
        return 0;
    }
}
