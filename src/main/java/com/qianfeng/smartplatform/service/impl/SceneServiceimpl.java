package com.qianfeng.smartplatform.service.impl;

import com.qianfeng.smartplatform.cache.SceneCache;
import com.qianfeng.smartplatform.exceptions.AddDataException;
import com.qianfeng.smartplatform.exceptions.ResultCode;
import com.qianfeng.smartplatform.mapper.SceneMapper;
import com.qianfeng.smartplatform.pojo.CheckType;
import com.qianfeng.smartplatform.pojo.Scene;
import com.qianfeng.smartplatform.pojo.User;
import com.qianfeng.smartplatform.service.SceneService;
import com.qianfeng.smartplatform.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2022/1/6 20:59
 *@Description:
 */
@Service
@Transactional
public class SceneServiceimpl implements SceneService {
    private SceneCache sceneCache;

    @Autowired
    public void setSceneCache(SceneCache sceneCache) {
        this.sceneCache = sceneCache;
    }

    private SceneMapper sceneMapper;

    @Autowired
    public void setSceneMapper(SceneMapper sceneMapper) {
        this.sceneMapper = sceneMapper;
    }

    @Override
    public void addScene(Scene scene) {
        Assert.isTrue(!scene.isEmpty(CheckType.ADD), () -> {
            throw new AddDataException("必须传递的数据不完整，请检查", ResultCode.DATA_NULL);
        });
        //我们的场景是否允许名字重复，注意是指的同一个用户是否可以创建多个相同名字的场景
        //不同用户的场景名字可以重复，所以表中的列肯定不能有唯一性约束
        //假如我们不允许重复
        User user = SecurityUtils.getUser();
        //查询的数据
        Scene currentScene = sceneMapper.selectByUserIdAndName(user.getUId(), scene.getSceneName());
        Assert.isTrue(currentScene == null || currentScene.getStatus() != 1, () -> {
            throw new AddDataException("场景已经存在", ResultCode.DATA_ALREADY_EXIST);
        });
        //更新或者删除插入
        //比如我们约定如果存在就更新数据，如果不存在就添加数据
        if (currentScene == null) {
            scene.setCreateBy(user.getUId());
            scene.setCreateTime(new Date());
            sceneMapper.addScene(scene);
        } else {
            currentScene.setCreateTime(new Date());
            sceneMapper.reEnableScene(currentScene.getSceneId(), new Date());
        }

    }

    @Override
    public void updateScene(Scene scene) {
        Assert.isTrue(!scene.isEmpty(CheckType.UPDATE), () -> {
            throw new AddDataException("必须传递的数据不完整，请检查", ResultCode.DATA_NULL);
        });
        sceneMapper.updateScene(scene);
        //因为更新数据可能是牵扯到有效的数据变成了无效的，所以要清理缓存
        sceneCache.getSceneLoadingCache().invalidateAll();//清理缓存
    }

    @Override
    public void deleteSceneByIds(List<Long> ids, Long userId) {
        //禁用后清理缓存
    }

    @Override
    public List<Scene> findByUserId(Long userId) {
        //缓存的问题
        //我们应该先查询缓存，缓存中没有则查询数据库
        //我们的缓存和之前的缓存不一样，不会直接全部查询出来保存，因为数据可能太多
        try {
            List<Scene> sceneList = sceneCache.getSceneLoadingCache().get(userId);//通过缓存获取数据
            return sceneList;
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }
}
