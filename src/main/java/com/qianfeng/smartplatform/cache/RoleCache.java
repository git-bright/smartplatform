package com.qianfeng.smartplatform.cache;

import com.qianfeng.smartplatform.events.RoleChangeEvent;
import com.qianfeng.smartplatform.mapper.RoleMapper;
import com.qianfeng.smartplatform.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sun.util.resources.ga.LocaleNames_ga;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2021/12/29 10:44
 *@Description:
 */
@Component
public class RoleCache extends BaseCache<Long,Role, RoleChangeEvent>{


/*    public RoleCache() {
        init();
    }*/

/*    private Map<Long, Role> valueMap = new HashMap<>();

    private List<Role> allRoles = new ArrayList<>();

    public List<Role> getAllRoles() {
        return allRoles;
    }


    public Role getValue(Long key) {
        return valueMap.get(key);
    }*/

    private RoleMapper roleMapper;

    @Autowired
    public void setRoleMapper(RoleMapper roleMapper) {
        this.roleMapper = roleMapper;
    }

    @PostConstruct//在构造方法之后执行
    @Override
    public void init() {
        //查询数据库
        //把数据添加到集合中
        List<Role> roleList = roleMapper.selectAllRoles();//查询了所有的数据
        List<Role> allRoles = getAllData();
        allRoles.clear();//清理原先的数据,目的是在发生变化的时候执行
        allRoles.addAll(roleList);//将新的数据加到集合


/*        for (Role role : roleList) {
            valueMap.put(role.getRoleId(),role);
        }*/
        Map<Long, Role> valueMap = getValueMap();
        valueMap.clear();
        valueMap.putAll(allRoles.parallelStream().collect(Collectors.toMap(Role::getRoleId,role -> role)));
        System.err.println("角色的缓存加载完成" + roleList);
    }

    @Override
    public void onEvent(RoleChangeEvent roleChangeEvent) {
        super.onEvent(roleChangeEvent);
    }
}
