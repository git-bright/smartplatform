package com.qianfeng.smartplatform.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.collect.Maps;
import com.qianfeng.smartplatform.mapper.SceneMapper;
import com.qianfeng.smartplatform.pojo.Scene;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2022/1/6 21:22
 *@Description:
 */

@Component
public class SceneCache {
    private SceneMapper sceneMapper;

    @Autowired
    public void setSceneMapper(SceneMapper sceneMapper) {
        this.sceneMapper = sceneMapper;
    }

    private LoadingCache<Long, List<Scene>> sceneLoadingCache = CacheBuilder.newBuilder()
            .expireAfterAccess(10, TimeUnit.MINUTES)//设置在最后一次访问之后10分钟过期
            .build(new CacheLoader<Long, List<Scene>>() {
                //设置缓存加载器，当缓存中没有数据的时候会通过加载器加载数据
                //我们加载缓存中的数据时候应该通过什么类型的关键数据加载，第一个泛型就是我们查询数据库的条件
                //第二个泛型就是我们查询后的数据返回类型是什么
                @Override
                public List<Scene> load(Long uid) throws Exception {
                    //我们在外面查询缓存的时候如果缓存中没有数据，会执行当前方法
                    List<Scene> sceneList = sceneMapper.selectByUserId(uid);
                    return sceneList;
                }
            });




    //缓存用什么保存？我们的缓存中保存的是每个用户的场景，通过用户的id查询的，所以是一个key_value结构
    //所以应该使用map来保存，但是map没有定时删除数据的操作，我们可以通过定时任务删除，但是定时任务要删除哪个数据
    //缓存穿透


    public LoadingCache<Long, List<Scene>> getSceneLoadingCache() {
        return sceneLoadingCache;
    }
}
