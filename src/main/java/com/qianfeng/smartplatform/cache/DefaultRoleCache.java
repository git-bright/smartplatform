package com.qianfeng.smartplatform.cache;

import com.qianfeng.smartplatform.events.DefaultRoleChangeEvent;
import com.qianfeng.smartplatform.mapper.DefaultRoleMapper;
import com.qianfeng.smartplatform.pojo.DefaultRole;
import com.qianfeng.smartplatform.pojo.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2021/12/29 10:43
 *@Description:
 */
@Component
public class DefaultRoleCache extends BaseCache<String,DefaultRole, DefaultRoleChangeEvent> {


/*public DefaultRoleCache(){
    init();
}*/


    private DefaultRoleMapper defaultRoleMapper;

    @Autowired
    public void setDefaultRoleMapper(DefaultRoleMapper defaultRoleMapper) {
        this.defaultRoleMapper = defaultRoleMapper;
    }

/*
    private List<DefaultRole> allData = new ArrayList<>();

    public List<DefaultRole> getAllData() {
        return allData;
    }

//    上面的集合返回的是所有的数据，而我们想要单个数据所以用Map集合存储
//    方便我们通过关键key单独获取某个数据
    private Map<String, DefaultRole> valueMap = new HashMap<>();

    public DefaultRole getValue(String key) {
        return valueMap.get(key);
    }
*/

    @PostConstruct//在构造方法之后执行   相当于上面的无参构造方法中调用init();
    @Override
    public void init() {
        //查询数据库
        List<DefaultRole> allDefaultRoles = defaultRoleMapper.findAllDefaultRoles();//查询了所有的数据
        List<DefaultRole> allData = getAllData();//获取保存缓存的集合
        allData.clear();//清理原先的数据，目的是在发生变化的时候执行
        //把数据添加到集合中
        allData.addAll(allDefaultRoles);//将新数据添加到集合

/*
        for (DefaultRole defaultRole : allDefaultRoles) {
            valueMap.put(defaultRole.getType(),defaultRole);
        }
*/
        Map<String, DefaultRole> map = allData.parallelStream().collect(Collectors.toMap(DefaultRole::getType, defaultRole -> defaultRole));
        Map<String, DefaultRole> valueMap = getValueMap();//获取map类型的缓存，方便单个查询
        valueMap.clear();
        //将集合的数据再保存一份到map中
        valueMap.putAll(map);

        System.err.println("默认角色已经加载完成" + allDefaultRoles);
    }

    @Override
    public void onEvent(DefaultRoleChangeEvent defaultRoleChangeEvent) {
        super.onEvent(defaultRoleChangeEvent);
    }
}
