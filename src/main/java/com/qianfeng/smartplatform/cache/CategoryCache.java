package com.qianfeng.smartplatform.cache;

import com.qianfeng.smartplatform.events.CategoryChangeEvent;
import com.qianfeng.smartplatform.mapper.CategoryMapper;
import com.qianfeng.smartplatform.pojo.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * ´´´´´´´´██´´´´´´´
 * ´´´´´´´████´´´´´´
 * ´´´´´████████´´´´
 * ´´`´███▒▒▒▒███´´´´´
 * ´´´███▒●▒▒●▒██´´´
 * ´´´███▒▒▒▒▒▒██´´´´´
 * ´´´███▒▒▒▒██´
 * ´´██████▒▒███´´´´´
 * ´██████▒▒▒▒███´´
 * ██████▒▒▒▒▒▒███´´´´
 * ´´▓▓▓▓▓▓▓▓▓▓▓▓▓▒´´
 * ´´▒▒▒▒▓▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒▒´´▓▓▓▓▓▓▓▓▒´´´´´
 * ´.▒▒´´´´▓▓▓▓▓▓▓▒
 * ..▒▒.´´´´▓▓▓▓▓▓▓▒
 * ´▒▒▒▒▒▒▒▒▒▒▒▒
 * ´´´´´´´´´███████´´´´´
 * ´´´´´´´´████████´´´´´´´
 * ´´´´´´´█████████´´´´´´
 * ´´´´´´██████████´´´´             大部分人都在关注你飞的高不高，却没人在乎你飞的累不累，这就是现实！
 * ´´´´´´██████████´´´                     我从不相信梦想，我，只，相，信，自，己！
 * ´´´´´´´█████████´´
 * ´´´´´´´█████████´´´
 * ´´´´´´´´████████´´´´´
 * ________▒▒▒▒▒
 * _________▒▒▒▒
 * _________▒▒▒▒
 * ________▒▒_▒▒
 * _______▒▒__▒▒
 * _____ ▒▒___▒▒
 * _____▒▒___▒▒
 * ____▒▒____▒▒
 * ___▒▒_____▒▒
 * ███____ ▒▒
 * ████____███
 * █ _███_ _█_███
 * ——————————————————————————女神保佑，代码无bug——————————————————————
 */

/*
 *@author malixiaowu
 *@Date:2021/12/30 20:34
 *@Description:
 */
@Component
@EnableAsync //支持异步的支持，不开启  @Async 注解不可以使用
public class CategoryCache extends BaseCache<Long, Category,CategoryChangeEvent> {
    private CategoryMapper categoryMapper;

    @Autowired
    public void setCategoryMapper(CategoryMapper categoryMapper) {
        this.categoryMapper = categoryMapper;
    }

    @PostConstruct
    @Async
    @Override
    public void init() {
        List<Category> categoryList = categoryMapper.selectAllEnableCategories();
        List<Category> allData = getAllData();
        allData.clear();
        allData.addAll(categoryList);
        Map<Long, Category> valueMap = getValueMap();
        valueMap.clear();
        valueMap.putAll(allData.parallelStream().collect(Collectors.toMap(Category::getCId, category -> category)));
    }

//    @EventListener//当前方法是一个事件方法，专门处理CategoryChangeEvent类型的事件   /*观察者设计模式*/
//    @Async//当前方法在异步线程中执行，我们的业务代码可以正常结束
//    public void onEvent(CategoryChangeEvent categoryChangeEvent) {
//        System.err.println("收到了分类变化的事件");
//        init();
//        System.err.println("重新加载了缓存");
//
//    }

    //map.put(CategoryChangeEvent.class,List<>集合中放的都是发送这个事件com.qianfeng.smartplatform.cache.CategoryCache.onEvent的方法 )

//泛型擦除，泛型的实际作用是在写代码期间避免类型转换等操作的，实际运行的时候泛型会被擦除（泛型变为Object类型，需要重写接收指定类型的事件，否则会接收所有类型的事件）
    @Override
    public void onEvent(CategoryChangeEvent categoryChangeEvent) {
        super.onEvent(categoryChangeEvent);
    }
}
